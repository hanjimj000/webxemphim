<?php

use App\Http\Controllers\EpisodeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MovieController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [App\Http\Controllers\IndexController::class, 'index'])->name('home-page');
Route::get('/danh-muc/{slug}', [App\Http\Controllers\IndexController::class, 'category'])->name('category');
Route::get('/the-loai/{slug}', [App\Http\Controllers\IndexController::class, 'genre'])->name('genre');
Route::get('/phim/{slug}', [App\Http\Controllers\IndexController::class, 'movie'])->name('movie');
Route::get('/xem-phim/{slug}', [App\Http\Controllers\IndexController::class, 'watch'])->name('watch');
Route::get('/quoc-gia/{slug}', [App\Http\Controllers\IndexController::class, 'country'])->name('country');
Route::get('/so-tap', [App\Http\Controllers\IndexController::class, 'episode'])->name('so-tap');
Route::get('/year/{slug}', [App\Http\Controllers\IndexController::class, 'year'])->name('year');
Route::post('/update-topview-phim', [MovieController::class,'update_topview']);
Route::post('/update-season-phim', [MovieController::class,'update_season']);
Route::get('/tim-kiem', [App\Http\Controllers\IndexController::class,'search'])->name('search');
Route::get('/select-movie', [EpisodeController::class,'select_movie'])->name('select-movie');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource('/', App\Http\Controllers\IndexController::class);
Route::resource('/category', App\Http\Controllers\CategoryController::class);
Route::resource('/genre', App\Http\Controllers\GenreController::class);
Route::resource('/movie', App\Http\Controllers\MovieController::class);
Route::resource('/watch', App\Http\Controllers\WatchController::class);
Route::resource('/country', App\Http\Controllers\CountryController::class);
Route::resource('/episode', App\Http\Controllers\EpisodeController::class);
