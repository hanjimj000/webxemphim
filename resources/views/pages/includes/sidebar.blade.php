<div class="col-md-4">

   <aside id="sidebar" class="col-xs-12 col-sm-12 col-md-4">
      <div id="halim_tab_popular_videos-widget-7" class="widget halim_tab_popular_videos-widget">
         <div class="section-bar clearfix">
            <div class="section-title">
               <span>Phim Hot</span>
              
            </div>
         </div>
         <section class="tab-content">
            <div role="tabpanel" class="tab-pane active halim-ajax-popular-post">
               <div class="halim-ajax-popular-post-loading hidden"></div>
               <div id="halim-ajax-popular-post" class="popular-post">
                 
                  @foreach ($phim_hot as $key =>$hot)
  
                  <div class="item post-37176">
                     <a href="{{url('xem-phim/'.$hot->slug.'?tap=1')}}" title="CHỊ MƯỜI BA: BA NGÀY SINH TỬ">
                        <div class="item-link">
                           <img src={{asset('upload/movie/'.$hot->image)}} class="lazy post-thumb" alt={{$hot->title}} title={{$hot->title}} />
                           <span class="is_trailer">
                               @if($hot->phim_hot ===1)
                                Phim Hot
  @else
  
  @endif
                           </span>
                        </div>
                        <p class="title">{{$hot->title}}</p>
                     </a>
                     <div class="viewsCount" style="color: #9d9d9d;">{{$hot->view}} lượt xem</div>
                     <div style="float: left;">
                        <span class="user-rate-image post-large-rate stars-large-vang" style="display: block;/* width: 100%; */">
                        <span style="width: 0%"></span>
                        </span>
                     </div>
                  </div>
                 
        
           @endforeach
                 
                 
                 
               </div>
            </div>
         </section>
         <div class="clearfix"></div>
      </div>
   </aside>
    <aside id="sidebar" class="col-xs-12 col-sm-12 col-md-4">
       <div id="halim_tab_popular_videos-widget-7" class="widget halim_tab_popular_videos-widget">
          <div class="section-bar clearfix">
             <div class="section-title">
                <span>Top Views</span>
                {{-- <ul class="halim-popular-tab" role="tablist">
                   <li role="presentation" class="active">
                      <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="today">Day</a>
                   </li>
                   <li role="presentation">
                      <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="week">Week</a>
                   </li>
                   <li role="presentation">
                      <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="month">Month</a>
                   </li>
                   <li role="presentation">
                      <a class="ajax-tab" role="tab" data-toggle="tab" data-showpost="10" data-type="all">All</a>
                   </li>
                </ul> --}}
  
                <ul class="nav  nav-pills mb-3" id="pills-tab" role="tablist">
                 <li class="nav-item active">
                   <a class="nav-link filter-sidebar active" style="font-size: 10px" id="pills-home-tab" data-toggle="pill" href="#ngay" role="tab" aria-controls="pills-home" aria-selected="true">Ngày</a>
                 </li>
                 <li class="nav-item filter-sidebar">
                   <a class="nav-link "  id="pills-profile-tab" style="font-size: 10px" data-toggle="pill" href="#tuan" role="tab" aria-controls="pills-profile" aria-selected="false">Tuần</a>
                 </li>
                 <li class="nav-item">
                   <a class="nav-link filter-sidebar" id="pills-contact-tab" style="font-size: 10px" data-toggle="pill" href="#thang" role="tab" aria-controls="pills-contact" aria-selected="false">Tháng</a>
                 </li>
               </ul>
               <div class="tab-content" id="pills-tabContent">
                 <div class="tab-pane fade in active" id="ngay" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="halim-ajax-popular-post-loading hidden"></div>
                    <div id="halim-ajax-popular-post" class="popular-post">
                      
                       @foreach($top_view_ngay as $key =>$top_view)
  
                       <div class="item post-37176">
                          <a href="{{url('xem-phim/'.$top_view->slug.'?tap=1')}}" title="{{$top_view->title}}">
                             <div class="item-link">
                                <img src="{{asset('upload/movie/'.$top_view->image)}}" class="lazy post-thumb" alt="{{$top_view->title}}" title="{{$top_view->title}}" />
                                <span class="is_trailer">Trailer</span>
                             </div>
                             <p class="title">{{$top_view->title}}</p>
                          </a>
                          <div class="viewsCount" style="color: #9d9d9d;">{{$top_view->view}} lượt xem</div>
                          
                       </div>
                       @endforeach
                     
                      
                    </div>
                 </div>
                 <div class="tab-pane fade" id="tuan" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="halim-ajax-popular-post-loading hidden"></div>
                    <div id="halim-ajax-popular-post" class="popular-post">
                       @foreach($top_view_tuan as $key =>$top_view)
  
                       <div class="item post-37176">
                          <a href="{{url('xem-phim/'.$top_view->slug.'?tap=1')}}" title="{{$top_view->title}}">
                             <div class="item-link">
                                <img src="{{asset('upload/movie/'.$top_view->image)}}" class="lazy post-thumb" alt="{{$top_view->title}}" title="{{$top_view->title}}" />
                                <span class="is_trailer">Trailer</span>
                             </div>
                             <p class="title">{{$top_view->title}}</p>
                          </a>
                          <div class="viewsCount" style="color: #9d9d9d;">{{$top_view->view}} lượt xem</div>
                        
                       </div>
                       @endforeach
                      
                      
                    </div>
                 </div>
                 <div class="tab-pane fade" id="thang" role="tabpanel" aria-labelledby="pills-contact-tab">
                    <div class="halim-ajax-popular-post-loading hidden"></div>
                    <div id="halim-ajax-popular-post" class="popular-post">
                       @foreach($top_view_thang as $key =>$top_view)
  
                       <div class="item post-37176">
                          <a href="{{url('xem-phim/'.$top_view->slug.'?tap=1')}}" title="{{$top_view->title}}">
                             <div class="item-link">
                                <img src="{{asset('upload/movie/'.$top_view->image)}}" class="lazy post-thumb" alt="{{$top_view->title}}" title="{{$top_view->title}}" />
                                <span class="is_trailer">Trailer</span>
                             </div>
                             <p class="title">{{$top_view->title}}</p>
                          </a>
                          <div class="viewsCount" style="color: #9d9d9d;">{{$top_view->view}} lượt xem</div>
                         
                       </div>
                       @endforeach
                    
                      
                      
                    </div>
                 </div>
               </div>
             </div>
          </div>
          
          <div class="clearfix"></div>
       </div>
    </aside>
</div>