@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                @if(!isset($episode))
                {!! Form::open(['route' => 'episode.store','method'=>'post','enctype'=>'multipart/form-data']) !!}
                @else
                {!! Form::open(['route' => ['episode.update',$episode->id],'method'=>'put','enctype'=>'multipart/form-data']) !!}
                @endif
                   
                    <div class="form-group">
                        {{ Form::label('movie', 'Chọn phim', ['class' => 'control-label']) }}
                        {{ Form::select('movie_id',['0'=>'Chọn phim','Phim mới nhất'=>$list], isset($episode)?$episode->movie_id:'' , array_merge(['class' => 'form-control select-movie'])) }}
                    
                    </div>
                    <div class="form-group">
                        {{ Form::label('linkphim', 'Link phim', ['class' => 'control-label']) }}
                        {{ Form::text('linkphim',isset($episode)?$episode->linkphim:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('episode', 'Tập phim', ['class' => 'control-label']) }}
                        {{-- {{ Form::text('episode',isset($episode)?$episode->episode:'' , array_merge(['class' => 'form-control'])) }} --}}
                      
                        @if(isset($episode))
                        {{ Form::text('episode',isset($episode)?$episode->episode:'' , array_merge(['class' => 'form-control '])) }}

                    @else
                    <select name="episode" id="show_movie" class="form-control">
                    </select>
@endif
                    </div>
                   

                    @if(!isset($episode))
                    {!! Form::submit('Thêm dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @else
                    {!! Form::submit('Sửa dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @endif
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-5">
           <div class="table-responsive">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Tên phim</th>
                    <th scope="col">Link phim</th>

                    <th scope="col">Tập phim</th>
                    
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($listEp as $key => $cate) 
                     {{-- {{$cate}} --}}
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                    {{-- <th scope="row" >{{$cate->title}}</th> --}}
                    {{-- <th scope="row">{{$cate->movie->title}}</th> --}}
                    <th scope="row">{!!$cate->linkphim!!}</th>
                    <th scope="row">{{$cate->episode}}</th>


                
            


                         <th>
                            <?php $edit?>
                            {!! Form::open(['method'=>'delete','route'=>['episode.destroy',$cate->id],'onsubmit'=>'return confirm("Xóa")']) !!}
                              {!! Form::submit('Xóa',['class'=>'btn btn-danger'] ) !!}
  
                      
                               {!! Form::close() !!}
  
                                <a href="{{route('episode.edit',$cate->id)}}" class="btn btn-warning">Sửa</a>
                         </th> 
                  </tr>
                            
                  @endforeach
                 
                 
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
@endsection
