@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                @if(!isset($genre))
                {!! Form::open(['route' => 'genre.store','method'=>'post']) !!}
                @else
                {!! Form::open(['route' => ['genre.update',$genre->id],'method'=>'put']) !!}
                @endif
                   
                    <div class="form-group">
                        {{ Form::label('title', 'Tiêu đề', ['class' => 'control-label']) }}
                        {{ Form::text('title',isset($genre)?$genre->title:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('slug', 'Slug', ['class' => 'control-label']) }}
                        {{ Form::text('slug',isset($genre)?$genre->slug:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('description', 'Mô tả', ['class' => 'control-label']) }}
                        {{ Form::textarea('description', isset($genre)?$genre->description:'', array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Trạng thái', ['class' => 'control-label']) }}
                        {{ Form::select('status', [1=>'Hiển thị',0=>'Không hiển thị'], isset($genre)?$genre->status:'') }}
                    </div>

                    @if(!isset($genre))
                    {!! Form::submit('Thêm dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @else
                    {!! Form::submit('Sửa dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @endif
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-5">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Tiêu đề</th>
                    <th scope="col">SLug</th>

                    <th scope="col">Mô tả</th>
                    <th scope="col">Trạng thái</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                
                    @foreach ($list as $key => $cate) 
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                    <th scope="row">{{$cate->title}}</th>
                    <th scope="row">{{$cate->slug}}</th>

                    <th scope="row">{{$cate->description}}</th>
                    <th scope="row">
                            @if ($cate->status)
                              Hiển thị
                            @else
Không hiển thị
                            @endif
                         </th>
                    <td>
                          <?php $edit?>
                          {!! Form::open(['method'=>'delete','route'=>['genre.destroy',$cate->id],'onsubmit'=>'return confirm("Xóa")']) !!}
                            {!! Form::submit('Xóa',['class'=>'btn btn-danger'] ) !!}

                    
                             {!! Form::close() !!}

                              <a href="{{route('genre.edit',$cate->id)}}" class="btn btn-warning">Sửa</a>
                    </td>
                  </tr>
                            
                  @endforeach
                 
                 
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
