@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                @if(!isset($movie))
                {!! Form::open(['route' => 'movie.store','method'=>'post','enctype'=>'multipart/form-data']) !!}
                @else
                {!! Form::open(['route' => ['movie.update',$movie->id],'method'=>'put','enctype'=>'multipart/form-data']) !!}
                @endif
                   
                    <div class="form-group">
                        {{ Form::label('title', 'Tiêu đề', ['class' => 'control-label']) }}
                        {{ Form::text('title',isset($movie)?$movie->title:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('slug', 'Slug', ['class' => 'control-label']) }}
                        {{ Form::text('slug',isset($movie)?$movie->slug:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                  
                  
                    <div class="form-group">
                        {{ Form::label('description', 'Mô tả', ['class' => 'control-label']) }}
                        {{ Form::textarea('description', isset($movie)?$movie->description:'', array_merge(['class' => 'form-control'])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('trailler', 'Trailler', ['class' => 'control-label']) }}
                        {{ Form::text('trailler', isset($movie)?$movie->trailler:'', array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Trạng thái', ['class' => 'control-label']) }}
                        {{ Form::select('status', [1=>'Hiển thị',0=>'Không hiển thị'], isset($movie)?$movie->status:'') }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phude', 'Phụ đề', ['class' => 'control-label']) }}
                        {{ Form::select('phude', [1=>'Thuyết minh',0=>'Phụ đề'], isset($movie)?$movie->phude:'') }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('phim_hot', 'Phim hot', ['class' => 'control-label']) }}
                        {{ Form::select('phim_hot', [1=>'Hiển thị',0=>'Không hiển thị'], isset($movie)?$movie->phim_hot:'') }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('image', 'Image', ['class' => 'control-label']) }}
                        {{ Form::file('image' , array_merge(['class' => 'form-control'])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('category_id', 'Danh mục', ['class' => 'control-label']) }}
                        {{ Form::select('category_id',$category,isset($movie)?$movie->slug:'' , array_merge(['class' => 'form-control'])) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('category_id', 'Thể loại', ['class' => 'control-label']) }}
                        <br/>
                        @foreach ($listGenre as $key => $gen)
                        @if(isset($movie))
                        {{ Form::checkbox('genre[]',$gen->id,$movie->genre_id===$gen->id?'checked':'') }}
@else
                        {{ Form::checkbox('genre[]',$gen->id) }}
                        @endif
                        {{ Form::label('genre_id', $gen ->title, ['class' => 'control-label']) }}

                        @endforeach
                    </div>
                    <div class="form-group">
                        {{ Form::label('country', 'Country', ['class' => 'control-label']) }}
                        {{ Form::select('country_id',$country,isset($movie)?$movie->slug:'' , array_merge(['class' => 'form-control'])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('year', 'Year', ['class' => 'control-label']) }}
                        {{ Form::text('year',isset($movie)?$movie->year:'' , array_merge(['class' => 'form-control'])) }}
                    </div>

                    <div class="form-group">
                        {{ Form::label('thoi_luong_phim', 'Thời lượng', ['class' => 'control-label']) }}
                        {{ Form::text('thoi_luong_phim',isset($movie)?$movie->thoi_luong_phim:'' , array_merge(['class' => 'form-control'])) }}
                    </div>

                    @if(!isset($movie))
                    {!! Form::submit('Thêm dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @else
                    {!! Form::submit('Sửa dữ liệu',['class'=>'btn btn-primary'] ) !!}

                    @endif
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-md-12 mt-5">
           <div class="table-responsive">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">STT</th>
                    <th scope="col">Tiêu đề</th>
                    <th scope="col">SLug</th>

                    <th scope="col">Image</th>

                    <th scope="col">Mô tả</th>
                    <th scope="col">trailler</th>

                    <th scope="col">Trạng thái</th>
                    <th scope="col">Phụ đề</th>

                    <th scope="col">Phim hot</th>

                    <th scope="col">Thể loại</th>
                    <th scope="col">Danh mục</th>
                    <th scope="col">Quốc gia</th>
                    <th scope="col">Năm</th>
                    <th scope="col">Thời lượng</th>
                    <th scope="col">Top View</th>
                    <th scope="col">Season</th>

                    <th scope="col">Ngày tạo</th>
                    <th scope="col">Ngày cập nhật</th>
                    
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($list as $key => $cate) 
                    <tr>
                        <th scope="row">{{$key+1}}</th>
                    <th scope="row" >{{$cate->title}}</th>
                    <th scope="row" >{{$cate->slug}}</th>

                    <th scope="row"><img width="40" src="{{asset('upload/movie/'.$cate->image)}}"/></th>

                    <th scope="row">{{$cate->description}}</th>
                    <th scope="row">{{$cate->trailler}}</th>

                    <th scope="row">
                            @if ($cate->status)
                              Hiển thị
                            @else
Không hiển thị
                            @endif
                         </th>

                         <th scope="row">
                            @if ($cate->phude)
                              Thuyết minh
                            @else
Phụ đề
                            @endif
                         </th>

                         <th scope="row">
                            @if ($cate->phim_hot)
                             Hot
                            @else
Không
                            @endif
                         </th>
                        <th scope="row">
                            @foreach ($cate->movie_genre as $key =>$mov_gen)
                            <button type="button" class="btn btn-dark"> {{$mov_gen->title}}</button>
                           
                            @endforeach
                        </th>
                        <th scope="row">{{$cate->genre->title}}</th>
                        <th scope="row">{{$cate->country->title}}</th>
                        <th scope="row">{{$cate->year}}</th>
                        <th scope="row">{{$cate->thoi_luong_phim}}</th>
                       
<th scope="row">
    <form method="post">
        @csrf
        {{ Form::select('top_view', [0=>'Ngày',1=>'Tuần',2=>'Tháng'], isset($cate->top_view)?$cate->top_view:'', array_merge(['class' => 'select_top_view','id'=>$cate->id])) }}

    </form>
</th>

<th scope="row">
    <form method="post">
        @csrf
        {{ Form::selectRange('season', 0,20, isset($cate->season)?$cate->season:'0', array_merge(['class' => 'select-season','id'=>$cate->id])) }}

    </form>

</th>
                        <th scope="row">{{$cate->ngay_tao}}</th>
                        <th scope="row">{{$cate->ngay_cap_nhat}}</th>

                         <th>
                            <?php $edit?>
                            {!! Form::open(['method'=>'delete','route'=>['movie.destroy',$cate->id],'onsubmit'=>'return confirm("Xóa")']) !!}
                              {!! Form::submit('Xóa',['class'=>'btn btn-danger'] ) !!}
  
                      
                               {!! Form::close() !!}
  
                                <a href="{{route('movie.edit',$cate->id)}}" class="btn btn-warning">Sửa</a>
                         </th>
                  </tr>
                            
                  @endforeach
                 
                 
                </tbody>
              </table>
            </div>
        </div>
    </div>
</div>
@endsection
