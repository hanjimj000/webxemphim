<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\Episode;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieGenre;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    //

      public function index(){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $category_home = Category::with('movie')-> orderBy('id','desc')->where('status',1)->get();
        $phim_hot  = Movie::where('phim_hot',1)->get();

        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();

        


        // return $phim_hot;
        return view("pages.home",compact('list','genre','country','category_home','phim_hot','top_view_ngay','top_view_tuan','top_view_thang'));
    }

    public function category($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $cate_slug = Category::where('slug',$slug) -> first();
        $phim_hot  = Movie::where('phim_hot',1)->get();
        $movie_year  = Movie::where('slug',$slug)->get();
        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();
        // return $list;
    //   return $cate_slug->id;
    if (!$cate_slug) {
        abort(404); // Hoặc làm điều gì đó khác để xử lý trường hợp không tìm thấy danh mục
    }
        $movie = Movie::where('category_id',$cate_slug->id)->get();
        return view("pages.category",compact('list','genre','country','movie','cate_slug','phim_hot','movie_year','top_view_ngay','top_view_tuan','top_view_thang'));
    }

    public function year($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $year = $slug;
        $movie_year  = Movie::where('year',$slug)->orderBy('ngay_cap_nhat','desc')->get();
        $phim_hot  = Movie::where('phim_hot',1)->get();


        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();
        // return $movie;
        return view("pages.year",compact('list','genre','country','movie_year','phim_hot','top_view_ngay','top_view_tuan','top_view_thang'));
    }

    public function movie($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $detail_movie = Movie::with('category','genre','country','movie_genre')-> where('slug',$slug)->first();
        $related = Movie::with('category','genre','country')->where('category_id',$detail_movie->category->id)->orderBy(DB::raw('RAND()'))->whereNotIn('slug',[$slug])->get();
        // return $detail_movie;
        $phim_hot  = Movie::where('phim_hot',1)->get();
        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();
       
        // return $detail_movie;
        return view("pages.movie",compact('list','genre','country','detail_movie','related','phim_hot','top_view_ngay','top_view_tuan','top_view_thang'));
    }

    public function genre($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $genre_slug = Genre::where('slug',$slug) -> first();
        $phim_hot  = Movie::where('phim_hot',1)->get();

        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();

        
       $movie_genre = MovieGenre::where('genre_id',$genre_slug->id)->get();

       $many_genre=[];
       foreach($movie_genre as $key =>$mov){
        $many_genre[] = $mov->movie_id;
       }

       $movie = Movie::whereIn('id',$many_genre)->orderBy('ngay_cap_nhat','desc')->paginate(40);
        return view("pages.genre",compact('list','genre','country','genre_slug','phim_hot','top_view_ngay','top_view_tuan','top_view_thang','movie'));
    }
    public function watch($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $genre_slug = Genre::where('slug',$slug) -> first();
        $phim_hot  = Movie::where('phim_hot',1)->get();

        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();


       
        $movie = Movie::with('category','genre','country','movie_genre','episode')-> where('slug',$slug)->first();
       
        $related = Movie::with('category','genre','country')->where('category_id',$movie->category->id)->orderBy(DB::raw('RAND()'))->whereNotIn('slug',[$slug])->get();

        //  return $movie;
        return view("pages.watch",compact('list','movie','genre','country','genre_slug','phim_hot','top_view_ngay','top_view_tuan','top_view_thang','related'));
    }
    public function country($slug){
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $country_slug = Country::where('slug',$slug) -> first();
        $phim_hot  = Movie::where('phim_hot',1)->get();
        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();
        return view("pages.country",compact('list','genre','country','country_slug','phim_hot','top_view_ngay','top_view_tuan','top_view_thang'));
    }

    public function search(Request $request)
    {
        //
        $list = Category::orderBy('id','desc')->where('status',1)->get();
        $genre = Genre::orderBy('id','desc')->get();
        $country = Country::orderBy('id','desc')->get();
        $phim_hot  = Movie::where('phim_hot',1)->get();
        $top_view_ngay = Movie::where('top_view','0')->get();
        $top_view_tuan = Movie::where('top_view','1')->get();
        $top_view_thang = Movie::where('top_view','2')->get();

        // $list = Movie::with('category','genre','country')->orderBy('id','desc')->get();

        $query = $request->input('s');
        $movies = Movie::where('title', 'like', "%$query%")->get();
        // $path = public_path()."/json/";

        // if(!is_dir($path)){
        //     mkdir($path,0777,true);
        // }
        // File::put($path."movies.json",json_encode($list));
        return view("pages.search",compact('list','movies','genre','country','phim_hot','top_view_ngay','top_view_tuan','top_view_thang'));

    }
    public function episode(){    
        return view("pages.episode",compact('list'));
    }
}
