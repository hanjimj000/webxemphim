<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Country;
use App\Models\Episode;
use App\Models\Genre;
use App\Models\Movie;
use App\Models\MovieGenre;
use Faker\Core\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //

     

    }
  
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
        $category = Category::pluck('title','id');
        $genre = Genre::pluck('title','id');
        $country = Country::pluck('title','id');
      
        $list = Movie::with('category','genre','country','movie_genre')->orderBy('id','desc')->get();

        //  return $list;
        $listGenre = Genre::all();
        // $path = public_path()."/json/";

        // if(!is_dir($path)){
        //     mkdir($path,0777,true);
        // }
        // File::put($path."movies.json",json_encode($list));
        // return $list;
      
        return view('admincp.movie.form',compact('list','category','genre','country','listGenre'));

    }
     
    public function update_topview(Request $request){
          $data  = $request->all();
          $movie = Movie::find($data['id_phim']);
          $movie ->top_view = $data['top_view'];
          $movie->save();
    }

    public function update_season(Request $request){
        $data  = $request->all();
        $movie = Movie::find($data['id_phim']);
        $movie ->season = $data['season'];
        return  $movie;
        $movie->save();
  }
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //   
        $data = $request ->all();

        $movie = new Movie();
          
        $movie->title = $data['title'];
        $movie->slug = $data['slug'];
        $movie->view = rand(0,5000);

        $movie->status = $data['status'];
        $movie->phude = $data['phude'];
        $movie->trailler = $data['trailler'];

        $movie->phim_hot = $data['phim_hot'];
        $movie->year = $data['year'];
        $movie->thoi_luong_phim = $data['thoi_luong_phim'];

        $movie->description = $data['description'];
        $movie->category_id = $data['category_id'];
        // $movie->genre_id = $data['genre_id'];
        $movie->country_id = $data['country_id'];
        $movie->ngay_tao = Carbon::now('Asia/Ho_Chi_Minh');
        $movie->ngay_cap_nhat = Carbon::now('Asia/Ho_Chi_Minh');
        $get_image = $request->file('image');

        foreach($data['genre'] as $key => $gen){
            $movie ->genre_id = $gen[0];
         }

        // return $data['genre_id'];
        $path  = 'upload/movie';

         if($get_image){
            $get_name_image  = $get_image->getClientOriginalName();

            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,9999).'.'.$get_image->getClientOriginalExtension();
            $get_image->move($path,$new_image);
            $movie->image = $new_image;

         }


        //   return $movie;
         $movie ->save();

         $movie->movie_genre()->attach($data['genre']);


        return redirect()->back();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //

        $category = Category::pluck('title','id');
        $list = Movie::with('category','genre','country')-> orderBy('id','desc')->get();
        $genre = Genre::pluck('title','id');
        $country = Country::pluck('title','id');
        $movie = Movie::find($id);
        $listGenre = Genre::all();

// return $list;
        return view('admincp.movie.form',compact('list','category','genre','country','movie','listGenre'));

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $data = $request ->all();

        $movie =  Movie::find($id);
          
        $movie->title = $data['title'];
        $movie->slug = $data['slug'];
        $movie->trailler = $data['trailler'];

        $movie->status = $data['status'];
        $movie->phude = $data['phude'];

        $movie->phim_hot = $data['phim_hot'];
        $movie->year = $data['year'];
        $movie->thoi_luong_phim = $data['thoi_luong_phim'];
        $movie->description = $data['description'];
        $movie->category_id = $data['category_id'];
        // $movie->genre_id = $data['genre_id'];
        $movie->country_id = $data['country_id'];
        $movie->ngay_cap_nhat = Carbon::now('Asia/Ho_Chi_Minh');

        $get_image = $request->file('image');

        $path  = 'upload/movie';

         if($get_image){
            $get_name_image  = $get_image->getClientOriginalName();

            $name_image = current(explode('.',$get_name_image));
            $new_image = $name_image.rand(0,9999).'.'.$get_image->getClientOriginalExtension();
            $get_image->move($path,$new_image);
            $movie->image = $new_image;

         }

         foreach($data['genre'] as $key => $gen){
            $movie ->genre_id = $gen[0];
         }



        //  return $movie;
         $movie ->save();
         $movie ->movie_genre()->sync($data['genre']);
         
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
        $movie = Movie::find($id);
        //  return $movie;
         MovieGenre::whereIn('movie_id',[$movie->id])->delete();
         Episode::whereIn('movie_id',[$movie->id])->delete();

        $movie ->delete();

        return redirect()->back();
    }
}
