<?php

namespace App\Http\Controllers;

use App\Models\Episode;
use App\Models\Movie;
use Illuminate\Http\Request;

class EpisodeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
      
        $list = Movie::orderBy('id','desc')->pluck('title','id');
       
        $listEp = Episode::with('movie')->get();
        // return $listEp;
        return view('admincp.episode.form',compact('list','listEp'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $data = $request ->all();
         $ep = new Episode();
           
         $ep->movie_id = $data['movie_id'];
         $ep->linkphim = $data['linkphim'];
         $ep->episode = $data['episode'];
       
         $ep ->save();

         return redirect()->back();
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
        $list = Movie::orderBy('id','desc')->pluck('title','id');
        $listEp = Episode::with('movie')->get();

        $episode = Episode::find($id);
        // return   $episode;
        return view('admincp.episode.form',compact('listEp','episode','list'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $data = $request ->all();
        $ep = Episode::find($id);
          
        // return $data;
        $ep->movie_id = $data['movie_id'];
        $ep->linkphim = $data['linkphim'];
        $ep->episode = $data['episode'];
      
        $ep ->save();

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function select_movie()
    {
        //
        $id = $_GET['id'];

        $movie = Movie::find($id);

        $arr='<option>Chọn tập phim</option>';

          for($i = 1;$i<=$movie->sotap;$i++){
            $arr.='<option value="'.$i.'">'.$i.'</option>';
          }


          echo $arr;
    }
}
